package bellpa.example.todantutorial;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.container)
    FrameLayout container;

    private Fragment[] FragmentPager = new Fragment[]{new FirstFragment(), new SecondFragment(), new ThirdFragment()};

    @BindView(R.id.drawer_layout)
    DrawerLayout dlDrawer;
    ActionBarDrawerToggle dtToggle;
    @BindView(R.id.drawer)
    View drawer;
    @BindView(R.id.pager_main)
    ViewPager pager_main;

    @BindView(R.id.relative_1)
    RelativeLayout relative_1;
    @BindView(R.id.relative_2)
    RelativeLayout relative_2;
    @BindView(R.id.relative_3)
    RelativeLayout relative_3;

    private Context mCotext = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mCotext = this;

        init();
    }

    private void init(){
        dtToggle = new ActionBarDrawerToggle(this, dlDrawer, R.string.app_name, R.string.app_name);
        dlDrawer.addDrawerListener(drawerListener);

        pager_main.setCurrentItem(0);
        pager_main.setAdapter(new HomePager(getSupportFragmentManager()));
        pager_main.setOnPageChangeListener(changeListener);

    }

    class HomePager extends FragmentPagerAdapter {

        public HomePager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // 해당하는 page의 Fragment를 생성합니다.
            return FragmentPager[position];
        }

        @Override
        public int getCount() {
            return 3;  // 총 3개의 page를 보여줍니다.
        }
    }

    DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(View drawerView) {

            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }

        @Override
        public void onDrawerClosed(View drawerView) {
        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    private ViewPager.OnPageChangeListener changeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {

            if (position == 0) {
                relative_1.setBackgroundColor(mCotext.getResources().getColor(R.color.color_FFC90E));
                relative_2.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                relative_3.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));

            } else if (position == 1) {
                relative_1.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                relative_2.setBackgroundColor(mCotext.getResources().getColor(R.color.color_FFC90E));
                relative_3.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));

            } else if (position == 2) {
                relative_1.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                relative_2.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                relative_3.setBackgroundColor(mCotext.getResources().getColor(R.color.color_FFC90E));

            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    @OnClick({R.id.relative_1, R.id.relative_2, R.id.relative_3})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.relative_1:
                relative_1.setBackgroundColor(mCotext.getResources().getColor(R.color.color_FFC90E));
                relative_2.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                relative_3.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                pager_main.setCurrentItem(0);
                break;
            case R.id.relative_2:
                relative_1.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                relative_2.setBackgroundColor(mCotext.getResources().getColor(R.color.color_FFC90E));
                relative_3.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                pager_main.setCurrentItem(1);
                break;
            case R.id.relative_3:
                relative_1.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                relative_2.setBackgroundColor(mCotext.getResources().getColor(R.color.color_ffffff));
                relative_3.setBackgroundColor(mCotext.getResources().getColor(R.color.color_FFC90E));
                pager_main.setCurrentItem(2);
                break;
            default:
                break;
        }
    }

}