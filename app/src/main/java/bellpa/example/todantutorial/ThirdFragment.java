package bellpa.example.todantutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;


/**
 * 세번째 프레그먼트
 * Created by BellPa on 2016-12-09.
 */
public class ThirdFragment extends Fragment {

    protected final String TAG = this.getClass().getSimpleName();

    private View mRoot;
    private View footer;

    public static ThirdFragment newInstance(String response) {
        ThirdFragment f = new ThirdFragment();
        Bundle args = new Bundle();
        args.putString("Response", response);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        mRoot = inflater.inflate(R.layout.fragment_tab_third, null);
        ButterKnife.bind(this, mRoot);

        return mRoot;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        // this is really important in order to save the state across screen
        // configuration changes for example
        setRetainInstance(true);
        loadArgments();

        init();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void loadArgments() {
        try {
            Bundle args = getArguments();
            String response = args.getString("Response");

        } catch (Exception ex) {
//			ex.printStackTrace();
        }
    }

    private void init() {

        //DB 객체 생성

    }

}
